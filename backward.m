function kdata = backward( im, smaps)
% from image to kspace
[Nx Ny Nframes] = size(im);
Ncoils = size(smaps,3);

kdata =single(zeros(Nx,Ny,Ncoils,Nframes));
for frame = 1:Nframes
    for coil=1:Ncoils
        kdata(:,:,coil,frame) =  chop_ifft2( smaps(:,:,coil).*im(:,:,frame) );
    end
end


function im = chop_ifft2( im)
[Nx Ny] = size(im);
persistent chop;
if isempty(chop)
    [x,y] = meshgrid(1:Ny,1:Nx);
    chop = (-1).^(x+y);
end
im = chop.*ifft2(chop.*im);