clear all;
clc;
close all;

%% load raw data
[kdata,idx,header]=read_ismrm_challenge_data('subject01.h5');

% k-space data: PE*FE*Ncoil*FRAME

test=size(kdata);
opyres=size(kdata,1);
opxres=size(kdata,2);
rhnframes=size(kdata,4);
ncoil=size(kdata,3);

%% sampling: training data
num_tra=16;
tra_line=(-8:7)+opyres/2+1;

kdata_tra=zeros(num_tra,opxres,ncoil,rhnframes);
im_tra=zeros(num_tra,opxres,ncoil,rhnframes);

im_tra_sum=zeros(num_tra,opxres,rhnframes);
T0=zeros(num_tra,opxres,rhnframes);

for nn_frame=1:rhnframes

    for nn_coil=1:ncoil
        
        kdata_tra(:,:,nn_coil,nn_frame)=kdata(tra_line,:,nn_coil,nn_frame);

        im_tra(:,:,nn_coil,nn_frame)=ift(kdata_tra(:,:,nn_coil,nn_frame));

    end
    
    tmp=squeeze(im_tra(:,:,:,nn_frame));
    
    im_tra_sum(:,:,nn_frame)=sqrt(sum(tmp.*conj(tmp),3)/ncoil);

end

%% training
T=reshape(im_tra_sum,num_tra*opxres,rhnframes);
%SVD
thresh=0.004;

disp('SVD');
[u,s,v] = svd(T,0);
s= diag(s);
sN =s/max(abs(s));
idxS = sN > thresh;
vpp = v(:,idxS);
nV = sum(idxS);

Basis_tem=vpp;
npc=nV;

%% Sensitivity map
% idx_map=zeros(opyres,opxres,rhnframes);
% idx_map(tra_line,(-16:15)+opxres/2+1,:)=ones(16,32,rhnframes);
% 
% S_map = espirit(kdata,idx_map,3,0.01);

load('case01_smap2.mat');
figure;
for nn_coil=1:ncoil
        
    subplot(4,8,nn_coil);
    imshow(abs(S_map(:,:,nn_coil)),[]);
    axis equal;axis tight;
end

S_map0=S_map;
%% kt-PCA recon
%% step1: create v'*A'*b
im_sum_re = forward( kdata ,S_map0);

tmp=reshape(im_sum_re,opyres*opxres,rhnframes);
im_sum_re0=permute(tmp,[2 1]);

b_real=Basis_tem'*im_sum_re0; %dimension: npc*(nx*ny)

b_real=b_real(:);
%% create function v'*A'*A*v

savefile = 'kt_pcadata.mat';
idx_re=idx;
save(savefile, 'Basis_tem', 'idx_re','S_map0');
%%
init_ut=zeros(npc*opyres*opxres,1);
tic
ut = CG_xm2(b_real,init_ut);
toc

%% ut: 
UT=reshape(ut,npc,opyres*opxres);

Im_recon=UT'*Basis_tem';

Im_recon=reshape(Im_recon,opyres,opxres,rhnframes);

%%

figure;

for nn_frame=1:rhnframes
    
        Im_final(:,:,nn_frame)=abs(Im_recon(:,:,nn_frame));
        
        imshow(Im_final(:,:,nn_frame),[]);
        
        M(nn_frame)=getframe;

end
movie(M,5);

%% last step:

smaps_sos= sqrt(sum(abs(S_map).^2,3));
Im_final2 = zeros(size(Im_recon));
for frame =1:rhnframes
    Im_final2(:,:,frame) = abs(Im_final(:,:,frame)).*smaps_sos;
end

%%
fid = fopen('case01_2.dat','w');
fwrite(fid,Im_final,'float32');
fclose(fid);

