function [x,iter_total,resvec] = CG_xm2(b,x)

% CG Conjugate Gradient  
%   x = CG(Q,b,x) solves the equation Qx = b with an intial estimate of x
%   iteratively using the conjugate gradient algorithm. Q is required to be
%   positive definite. 
% 
%   iter_total: total number of iterations
%   resvec: norm(gradient) after every iter
% Author: Terrence Jao
% Date:   4-07-2013

%Initial Values

r = b - kt_pca_xm2(x); % function: b-Qx
p = r;

resvec=zeros(100,1);

for j = 1:1e3
    
    Qp=kt_pca_xm2(p);
    
    alpha = (r'*r)/(p'*Qp);
    %Updating Estimate of x
    x = x + alpha*p;
    r_next = r-alpha*Qp;
    
    %Criteria for Convergence
    resvec(j)=norm(r_next);
    
    if norm(r_next) < 1e-5
        iter_total=j;
        break;
    end
    
    beta = (r_next'*r_next)/(r'*r);
    %Updating Search Direction: p->search direction  r_next->gradient at
    %point(x)
    p = r_next + beta*p;
    r = r_next;
end

end


function b_hat = kt_pca_xm2(ut)

load('kt_pcadata.mat');
% Basis_tem,nx,ny,nt,idx_re,S_map

% INPUT:
% ut: npc*(nx*ny)
% Basis_tem: nt*npc
% OUTPUT:
% b: npc*(nx*ny)
ny=size(idx_re,1);
nx=size(idx_re,2);
nt=size(idx_re,3);

npc=size(Basis_tem,2);
ncoil=size(S_map0,3);

ut=reshape(ut,npc,(nx*ny));% ut: npc*(nx*ny)
Array_t= Basis_tem*ut; % nt*(nx*ny)
Array_t=Array_t.';% (nx*ny)*nt

Im=reshape(Array_t,ny,nx,nt); % whole estimated image

K=zeros(ny,nx,ncoil,nt);
K_un=zeros(ny,nx,ncoil,nt);
im_sum=zeros(ny,nx,nt);

K = backward( Im, S_map0);
 
for nn_coil=1:ncoil
    
    for nnt=1:nt
    
        K_un(:,:,nn_coil,nnt)=K(:,:,nn_coil,nnt).*idx_re(:,:,nnt);%undersample

    end
       
end

im_sum = forward( K_un , S_map0);

im_sum=reshape(im_sum,nx*ny,nt);

im_sum=im_sum.'; %dim: nt*(nx*ny)

b_hat=Basis_tem'*im_sum; % dim: npc*(nx*ny)

b_hat=b_hat(:);

end




