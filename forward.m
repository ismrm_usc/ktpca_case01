function im = forward( kdata , smaps)
%% Kdata to Image
[Nx Ny Ncoils Nframes] = size(kdata);
im=single(zeros(Nx,Ny,Nframes));
for coil=1:Ncoils
    for frame = 1:Nframes
        im(:,:,frame) = im(:,:,frame) + conj(smaps(:,:,coil)).*chop_fft2( kdata(:,:,coil,frame) );
    end
end

function im = chop_fft2( im)
[Nx Ny] = size(im);
persistent chop;
if isempty(chop)
    [x,y] = meshgrid(1:Ny,1:Nx);
    chop = (-1).^(x+y);
end
im = chop.*fft2(chop.*im);